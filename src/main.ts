import { createApp } from 'vue'
import App from '@/App.vue'
import router from '@/router'
import store from '@/store'
import { dealFake404Main } from '@/utils/dealFake404'
import axios from 'axios'
import vueAxios from 'vue-axios'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import locale from 'element-plus/lib/locale/lang/zh-cn'

const app = createApp(App)
app.use(store).use(router).use(vueAxios, axios).use(ElementPlus, { locale })
const appThis = app.mount('#app')
dealFake404Main(appThis.$router)
