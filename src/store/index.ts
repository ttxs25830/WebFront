import http from 'axios'
import { createStore } from 'vuex'
import { UPDATE_WALLPAPER_URL } from './mutations-types'

const store = createStore({
  state: {
    wallPaperURL: ''
  },
  getters: {},
  mutations: {
    updateWallPaperURL (state, url: string) {
      state.wallPaperURL = url
    }
  },
  actions: {
    async getWallPaper (ctx) {
      http
        .request({
          url: 'https://api.25830web.top:44380/wall-paper',
          method: 'get'
        })
        .then((resp) => {
          try {
            const obj = JSON.parse(resp.data)
            if ('url' in obj) {
              ctx.commit({
                type: UPDATE_WALLPAPER_URL,
                url: obj.url
              })
            } else {
              throw new Error('There is no url arg in server response')
            }
          } catch (err) {
            console.log(
              `Server WallPaper Api is not available by response error:\n${String(
                err
              )}`
            )
          }
        })
        .catch((err) => {
          console.log(
            `Server WallPaper Api is not available by http error:\n${String(
              err
            )}`
          )
        })
    }
  },
  modules: {}
})

export default store
