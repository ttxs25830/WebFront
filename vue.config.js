const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  configureWebpack: require('./webpack.config'),
  pages: {
    dealFake404: {
      entry: 'src/dealFake404Redirect.ts',
      template: 'public/index.html',
      filename: '404.html'
    },
    index: {
      entry: 'src/main.ts',
      template: 'public/index.html',
      filename: 'index.html'
    }
  }
})
