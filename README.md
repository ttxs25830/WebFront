# 25830web——前端
[默认站点](https://www.25830web.top)

## 部署情况

### Netlify

[![Netlify Status](https://api.netlify.com/api/v1/badges/a9f0d1e6-d017-4b62-8995-112849acd989/deploy-status)](https://ttxs25830site.netlify.app/)

### Github Pages

[![Github Page Status](https://github.com/ttxs25830/WebFront/actions/workflows/deploy.yml/badge.svg)](https://www.25830web.top/)
